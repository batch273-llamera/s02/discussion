// alert("Hello, B273!")

// without the use of objects, our students from before would be organized as follows if we are to record additional information about them

// create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

// create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

// create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

// create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];

// actions that students may perform will be lumped together
// called as procedural programming
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}

// This way of organizing employees is not well organized at all.
// This will become unmanageable when we add more employees or functions
// To remedy this, we will create objects


// ENCAPSULATION - organizing information for a certain variable
// spaghetti code - code that is poorly organized that it becomes almost impossible to work on

// Object literal - adding properties and methods
let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
   
    // Mini-activity 1
        // Create function/method that will get/compute the quarterly average of studentOne's grades.

    computeAvg() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    // getAve() {
    //     const sum = this.grades.reduce((a, b) => a + b, 0);
    //     const avg = (sum / this.grades.length) || 0;

    //     console.log(`${this.name}'s quarterly grade average is: ${avg}`); 
    // },

    // Mini-Activity 2
        // Create function willPass() will return true if average is >=85, and false if not

    // willPass() {
    //     if (this.getAve() >= 85) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    willPass() {
        return this.computeAvg() >= 85 ? true : false;

        // condition ? value if condition is true : value if condition is false
    },

    // Mini-Activity 3
        // Create a function called willPassWithHonors() that returns true if the student has passed and their average grade is >= 90. The function returns false if either one is met.

    // willPassWithHonors() {
    //     if(this.willPass() &&  this.computeAvg() >=90) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    willPassWithHonors() {
        return (this.willPass() && this.computeAvg() >= 90) ? true : false;
    }

}

console.log(`Student one's name is ${studentOne.name}`);
console.log(`Student one's email is ${studentOne.email}`);
console.log(`Student one's quarterly grade averages are ${studentOne.grades}`);
